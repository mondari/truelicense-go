package pbe

import (
	"encoding/base64"
	"fmt"
	"github.com/gogf/gf/v2/encoding/gcompress"
	"github.com/gogf/gf/v2/encoding/gxml"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	password   = "qinglu8888"
	iterations = 2005
	plainText  = "hello"
)

var (
	salt = []byte{0xce, 0xfb, 0xde, 0xac, 0x05, 0x02, 0x19, 0x71}
)

func TestEncryptBytes(t *testing.T) {
	encryptStr, err := Encrypt(password, salt, iterations, plainText)
	if err != nil {
		return
	}

	encryptBytes, err := EncryptBytes(password, salt, iterations, []byte(plainText))
	if err != nil {
		return
	}

	assert.Equal(t, encryptStr, base64.StdEncoding.EncodeToString(encryptBytes))
}

func TestDecryptBytes(t *testing.T) {
	encryptBytes, err := EncryptBytes(password, salt, iterations, []byte(plainText))
	if err != nil {
		return
	}

	decryptBytes, err := DecryptBytes(password, salt, iterations, encryptBytes)
	if err != nil {
		return
	}

	assert.Equal(t, plainText, string(decryptBytes))
}

func TestEncrypt(t *testing.T) {
	encryptStr, err := Encrypt(password, salt, iterations, plainText)
	if err != nil {
		return
	}
	decryptStr, err := Decrypt(password, salt, iterations, encryptStr)
	if err != nil {
		return
	}

	assert.Equal(t, plainText, decryptStr)
}

func TestSaveAndLoad(t *testing.T) {
	var licenseKeyPath string = "D:\\Workspace\\mondari\\truelicense\\license.lic"
	fileData := gfile.GetBytes(licenseKeyPath)
	decryptBytes, err := DecryptBytes(password, salt, iterations, fileData)
	if err != nil {
		return
	}

	unGzipBytes, err := gcompress.UnGzip(decryptBytes)
	if err != nil {
		return
	}
	fmt.Println("=====	unGzip	=====")
	fmt.Println(string(unGzipBytes))

	decode, err := gxml.Decode(unGzipBytes)
	if err != nil {
		return
	}
	fmt.Println("=====	decode	=====")
	fmt.Println(decode)
}
